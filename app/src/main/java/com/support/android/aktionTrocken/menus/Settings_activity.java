package com.support.android.aktionTrocken.menus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.support.android.aktionTrocken.R;

/**
 * Created by simon on 05.08.2015.
 */
public class Settings_activity extends AppCompatActivity {
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        ab.setDisplayHomeAsUpEnabled(true);
        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.settingsLv);

        // Defined Array values to show in ListView
        String[] values = new String[] { "Wochentage        >",
                "Uhrzeit                  >"
        };

        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                String  itemValue    = (String) listView.getItemAtPosition(position);

                // Show Alert
                //Toast.makeText(getApplicationContext(), "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG).show();
                if(itemPosition == 0){
                    Intent i = new Intent(getApplication(), Settings_day_activity.class);
                    startActivity(i);
                }
                if(itemPosition == 1){
                    Intent i = new Intent(getApplication(), Settings_time_activity.class);
                    startActivity(i);
                }

            }

        });

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> aFrequency = ArrayAdapter.createFromResource(this,
                R.array.frequency, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        aFrequency.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        loadSavedPreferences();

    }

    private void loadSavedPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean checkBoxValue = sharedPreferences.getBoolean("CheckBox_Value", true);
        int spinnerValue = sharedPreferences.getInt("Spinner_Value", 0);
        //Toast.makeText(getApplication(), "Spinner " + sharedPreferences.getInt("Spinner_Value", 0), Toast.LENGTH_SHORT).show();

        //String name = sharedPreferences.getString("storedName", "YourName");

        if (checkBoxValue) {
        } else {
        }


    }

    private void savePreferences(String key, boolean value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }

    private void savePreferencesSpinner(String key, int value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;

    }
}
