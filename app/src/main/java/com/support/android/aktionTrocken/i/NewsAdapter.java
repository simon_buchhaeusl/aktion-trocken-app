package com.support.android.aktionTrocken.i;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.support.android.aktionTrocken.R;

import java.util.ArrayList;

/**
 * Created by simon on 23.07.2015.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<News> mDataset;
    private static MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView label;
        TextView dateTime;
        ImageView cvIcon;

        public DataObjectHolder(View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.cvTitle);
            dateTime = (TextView) itemView.findViewById(R.id.cvText);
            cvIcon = (ImageView) itemView.findViewById(R.id.cvIcon);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public NewsAdapter(ArrayList<News> myDataset) {
        mDataset = myDataset;
    }

    public void clear() {
        this.mDataset.clear();
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_i_news, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.label.setText(mDataset.get(position).getNewsTitle());
        holder.dateTime.setText(mDataset.get(position).getNewsDesc());
        holder.cvIcon.setImageResource(mDataset.get(position).getNewsIcon());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void addItem(News news, int index) {
        mDataset.add(index, news);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}
