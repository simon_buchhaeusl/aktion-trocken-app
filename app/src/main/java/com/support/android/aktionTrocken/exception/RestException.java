package com.support.android.aktionTrocken.exception;

/**
 * Created by simon on 19.01.2016.
 */
public class RestException extends Exception{
    public RestException(String detailMessage) {
        super(detailMessage);
    }
}
