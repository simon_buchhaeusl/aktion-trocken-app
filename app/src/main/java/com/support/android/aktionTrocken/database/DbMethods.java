package com.support.android.aktionTrocken.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.support.android.aktionTrocken.entry.Entry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by simon on 12.08.2015.
 */
public class DbMethods {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { "ID", "DAY", "DATE", "DETAILDATE", "CONSUMPTION", "SITUATION", "ICON", "ICON2", "DEVICEID" };

    public DbMethods(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }



    public Entry createEntry(String day,String date,String detailDate,int consumption, int situation, int icon, int icon2, String deviceId) {
        ContentValues values = new ContentValues();
        values.put("DAY",day);
        values.put("DATE",date);
        values.put("DETAILDATE",detailDate);
        values.put("CONSUMPTION",consumption);
        values.put("SITUATION",situation);
        values.put("ICON",icon);
        values.put("ICON2",icon2);
        values.put("DEVICEID",deviceId);

        long insertId = database.insert("ENTRY", null, values);

        Cursor cursor = database.query("ENTRY", allColumns, "ID = "
                + insertId, null, null, null, null);
        cursor.moveToFirst();

        return cursorToEntry(cursor);
    }

    public List<Entry> getAllEntries() {
        List<Entry> EntriesList = new ArrayList<Entry>();
        EntriesList = new ArrayList<Entry>();

        Cursor cursor = database.query("ENTRY", allColumns, null, null, null,
                null, null);
        cursor.moveToFirst();

        if (cursor.getCount() == 0)
            return EntriesList;

        while (cursor.isAfterLast() == false) {
            Entry entry = cursorToEntry(cursor);
            EntriesList.add(entry);
            cursor.moveToNext();
        }

        cursor.close();

        return EntriesList;
    }

    private Entry cursorToEntry(Cursor cursor) {
        Entry entry = new Entry();
        entry.setId(cursor.getInt(0));
        entry.setDay(cursor.getString(1));
        entry.setDate(cursor.getString(2));
        entry.setDetailDate(cursor.getString(3));
        entry.setConsumption(cursor.getInt(4));
        entry.setSituation(cursor.getInt(5));
        entry.setIcon(cursor.getInt(6));
        entry.setIcon2(cursor.getInt(7));
        entry.setDeviceId(cursor.getString(8));

        return entry;
    }

}
