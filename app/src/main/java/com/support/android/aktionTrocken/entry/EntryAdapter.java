package com.support.android.aktionTrocken.entry;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
import com.support.android.aktionTrocken.R;
import com.support.android.aktionTrocken.exception.RestException;
import com.support.android.aktionTrocken.helper.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by simon on 20.07.2015.
 */

public class EntryAdapter extends BaseAdapter {
    public static final int AMOUNT_HISTORY_DAYS = 30;

    // Consume status
    private static final int CONSUME_NO_ENTRY = 2;
    private static final int CONSUME_ON = 1;
    private static final int CONSUME_VERZICHT = 0;

    // Colors
    private static final int COLOR_NOCONSUME_ON = Color.parseColor("#6DCDB8");
    private static final int COLOR_INACTVIVE = Color.parseColor("#A7A8AA");
    private static final int COLOR_CONSUME_ON = Color.parseColor("#FF808B");


    private String deviceId;
    private Entry actualEntry;
    private String url;
    private String port, userId;

    private List<Entry> entriesData = new ArrayList<>();

    private ImageView img_situation;

    private Context context;
    private MaterialSimpleListAdapter situationDialogAdapter;

    private EntryListFragment entryListFragment;

    public EntryAdapter(Context context, EntryListFragment entryListFragment) {
        this.entriesData = new ArrayList<>();
        this.context = context;
        this.entryListFragment = entryListFragment;

        situationDialogAdapter = new MaterialSimpleListAdapter(context);
        setDialogAdapter();
        loadPreferences();

        this.deviceId = getDeviceId(context);

        if (Helper.isNetworkAvailable(context)) {
            new getUserIdAndThenLoadAllTasks().execute();
        } else {
            showNoInternetToast(context);
        }


    }

    private void showNoInternetToast(Context context) {
        Toast.makeText(context, "Bitte aktivieren Sie Ihre Internetverbindung", Toast.LENGTH_SHORT).show();
    }

    private void reload() {
        notifyDataSetChanged();
    }


    /**
     * Loads initialy an empty dataset with the last AMOUNT_HISTORY_DAYS
     */
    public void initDataset() {
        for (int i = 0; i < AMOUNT_HISTORY_DAYS; i++) {

            Entry entry = new Entry(makeDummyEntry(i), Helper.getActualDateMinusDays(i), 2, 0, 0, R.drawable.ic_event, Helper.getDetailDate(i), deviceId);
            entriesData.add(entry);
        }
    }

    private String makeDummyEntry(int i) {
        String day = "";
        if (i == 0) {
            day = "Heute";
        }
        if (i == 1) {
            day = "Gestern";
        }
        if (i > 1) {
            DateFormat dateFormat = new SimpleDateFormat("EEEE", Locale.GERMAN);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -i);
            day = dateFormat.format(cal.getTime());
        }
        return day;

    }


    @Override
    public int getCount() {
        return entriesData.size();
    }

    @Override
    public Object getItem(int i) {
        return entriesData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return entriesData.get(i).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Entry entry = entriesData.get(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item_entry, parent, false);
        }

        TextView dayText = (TextView) convertView.findViewById(R.id.dayText);
        TextView dateText = (TextView) convertView.findViewById(R.id.dateText);

        // Populate the data into the template view using the data object
        dayText.setText(entry.getDay());
        dateText.setText(entry.getDate());

        this.img_situation = (ImageView) convertView.findViewById(R.id.img_situation);

        final Button consumeBtn = (Button) convertView.findViewById(R.id.consumBtn);
        final Button noConsumBtn = (Button) convertView.findViewById(R.id.noConsumBtn);

        consumeBtn.setTag(position);
        noConsumBtn.setTag(position);

        consumeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Helper.isNetworkAvailable(context)) {
                    showNoInternetToast(context);
                    return;
                }

                entry.setConsumption(CONSUME_ON);
                showSimpleList(entry);
                EntryAdapter.this.actualEntry = entry;
                reload();
            }
        });

        noConsumBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Helper.isNetworkAvailable(context)) {
                    showNoInternetToast(context);
                    return;
                }

                entry.setConsumption(0);
                entry.setSituation(0);
                EntryAdapter.this.actualEntry = entry;

                new updateEntriesTask().execute("http://" + url + port + "/entries");
                reload();

            }
        });

        changeColor(position, noConsumBtn, consumeBtn);
        setSituationImage(position);

        return convertView;
    }

    private void loadPreferences() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        url = sharedPreferences.getString("url", "");
        port = sharedPreferences.getString("port", "");
    }

    private void changeColor(int position, Button noConsumBtn, Button consumeBtn) {

        int consumption = entriesData.get(position).getConsumption();
        if (consumption == CONSUME_VERZICHT) {
            setConsumeVerzicht(noConsumBtn, consumeBtn);
        } else if (consumption == CONSUME_ON) {
            setConsumeOnColors(noConsumBtn, consumeBtn);
        } else if (consumption == CONSUME_NO_ENTRY) {
            setNoEntryColor(noConsumBtn, consumeBtn);
        }
    }

    private void setNoEntryColor(Button noConsumBtn, Button consumeBtn) {
        noConsumBtn.setBackgroundColor(COLOR_INACTVIVE);
        consumeBtn.setBackgroundColor(COLOR_INACTVIVE);
    }

    private void setConsumeVerzicht(Button noConsumBtn, Button consumeBtn) {
        noConsumBtn.setBackgroundColor(COLOR_NOCONSUME_ON);
        consumeBtn.setBackgroundColor(COLOR_INACTVIVE);
    }

    private void setConsumeOnColors(Button noConsumBtn, Button consumeBtn) {
        consumeBtn.setBackgroundColor(COLOR_CONSUME_ON);
        noConsumBtn.setBackgroundColor(COLOR_INACTVIVE);
    }

    private void setSituationImage(int position) {
        int situation = entriesData.get(position).getSituation();
        if (situation == 0) {
            img_situation.setVisibility(View.GONE);
        } else {
            img_situation.setVisibility(View.VISIBLE);
        }
        if (situation == 1) {
            img_situation.setImageResource(R.drawable.ic_home_2x);
        } else if (situation == 2) {
            img_situation.setImageResource(R.drawable.ic_cake_3x);
        } else if (situation == 3) {
            img_situation.setImageResource(R.drawable.ic_local_bar_36pt_2x);
        } else if (situation == 4) {
            img_situation.setImageResource(R.drawable.ic_restaurant_menu_3x);
        } else if (situation == 5) {
            img_situation.setImageResource(R.drawable.ic_more_horiz_36pt_2x);
        }
    }


    private String getDeviceId(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    private void showSimpleList(final Entry entry) {


        new MaterialDialog.Builder(this.context)
                .title("W�hle deine Situtation")
                .backgroundColor(Color.WHITE)
                .contentColor(Color.DKGRAY)
                .titleColor(Color.BLACK)
                .adapter(situationDialogAdapter, new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        MaterialSimpleListItem item = situationDialogAdapter.getItem(which);

                        String s = item.getContent().toString();
                        if (s.equals("Zuhause")) {
                            entry.setSituation(1);
                            //consumeBtn.setBackgroundColor(Color.parseColor("#FF808B"));

                        } else if (s.equals("Feierlichkeit")) {
                            entry.setSituation(2);

                        } else if (s.equals("Ausgehen")) {
                            entry.setSituation(3);

                        } else if (s.equals("Essen")) {
                            entry.setSituation(4);

                        } else if (s.equals("Sonstiges")) {
                            entry.setSituation(5);

                        }


                        new updateEntriesTask().execute("http://" + url + port + "/entries");

                        dialog.dismiss();
                        //restart();
                        EntryAdapter.this.reload();
                    }


                })
                .show();
    }

    private void setDialogAdapter() {
        situationDialogAdapter.add(new MaterialSimpleListItem.Builder(context)
                .content("Zuhause")
                .icon(R.drawable.ic_home_3x)
                .build());
        situationDialogAdapter.add(new MaterialSimpleListItem.Builder(context)
                .content("Feierlichkeit")
                .icon(R.drawable.ic_cake_3x)
                .build());
        situationDialogAdapter.add(new MaterialSimpleListItem.Builder(context)
                .content("Ausgehen")
                .icon(R.drawable.ic_local_bar_36pt_2x)
                .build());
        situationDialogAdapter.add(new MaterialSimpleListItem.Builder(context)
                .content("Essen")
                .icon(R.drawable.ic_restaurant_menu_3x)
                .build());
        situationDialogAdapter.add(new MaterialSimpleListItem.Builder(context)
                .content("Sonstiges")
                .icon(R.drawable.ic_more_horiz_36pt_2x)
                .build());
    }


    private class updateEntriesTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            String result = Helper.updateEntryForUser(urls[0], EntryAdapter.this.actualEntry, EntryAdapter.this.userId);
            return result;
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Log.d("Aktion Trocken", "Entry channged:" + result);
        }

    }


    private class loadAllEntriesTask extends AsyncTask<String, Void, String> {
        private RestException restException = null;

        @Override
        protected String doInBackground(String... urls) {

            try {
                return Helper.getJSONFromRestCall("http://" + EntryAdapter.this.url + EntryAdapter.this.port + "/entries/" + EntryAdapter.this.userId);
            } catch (RestException e) {

                this.restException = e;
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            initDataset();
            if (restException != null) {
                showErrorToast(restException.getMessage());
            }

            writeResultToEntryList(result);
            EntryAdapter.this.notifyDataSetChanged();
            EntryAdapter.this.entryListFragment.hideProgressDialog();
        }
    }


    private class getUserIdAndThenLoadAllTasks extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            EntryAdapter.this.userId = Helper.getUserId("http://" + url + port + "/users/" + deviceId);
            return EntryAdapter.this.userId;
        }

        @Override
        protected void onPostExecute(String userId) {
            new loadAllEntriesTask().execute();
        }
    }

    private void writeResultToEntryList(String result) {
        Integer cnt = 0;
        try {
            JSONArray json = new JSONArray(result);

            for (int i = 0; i < json.length(); i++) {

                JSONObject jsonobject = json.getJSONObject(i);

                String consumptionString = jsonobject.getString("consumption");

                String date = jsonobject.getString("date");
                String situationString = jsonobject.getString("situation");

                int consumption = Integer.valueOf(consumptionString);
                int situation = Integer.valueOf(situationString);

                for (int k = 0; k < AMOUNT_HISTORY_DAYS; k++) {
                    String entriesDate = Helper.getDetailDate(k);
                    if (entriesDate.equals(date)) {
                        entriesData.get(k).setConsumption(consumption);
                        entriesData.get(k).setSituation(situation);
                        break;
                    }
                }
            }


        } catch (JSONException e) {

            throw new RuntimeException(e);
        }
    }

    private void showErrorToast(String message) {
        Toast.makeText(context, "Es ist ein Fehler aufgetreten! " + message, Toast.LENGTH_LONG).show();
    }


}
