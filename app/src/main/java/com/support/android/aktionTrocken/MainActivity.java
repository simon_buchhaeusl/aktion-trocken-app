package com.support.android.aktionTrocken;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.support.android.aktionTrocken.entry.EntryListFragment;
import com.support.android.aktionTrocken.group.GroupFragment;
import com.support.android.aktionTrocken.helper.Helper;
import com.support.android.aktionTrocken.i.IListFragment;

import java.util.Calendar;
import java.util.TimeZone;


public class MainActivity extends AppCompatActivity {

    SharedPreferences prefs = null;
    private String grpName, deviceId;
    private String strData = "";
    boolean countdown;
    TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean hasInternet = false;

        setDeviceID();


        if (Helper.isNetworkAvailable(this)) {
            initActivity();
        } else {
            setContentView(R.layout.activity_noconnection);
            Button retryBtn = (Button) findViewById(R.id.retryBtn);
            retryBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("Aktion Trocken","click");
                    if (Helper.isNetworkAvailable(MainActivity.this)){
                        initActivity();
                    }
                }
            });


        }


    }

    private void initActivity() {
        setContentView(R.layout.activity_main);

        savePreferences();
        setActionBar();
        setViewPager();
        checkFirstRun();

    }

    private void setDeviceID() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = telephonyManager.getDeviceId();
    }

    private void setActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(false);
    }

    private void setViewPager() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        //ToDo if abfrage mit INtent extra welches Fragment
        Uri data = getIntent().getData();
        if (data != null) {
            strData = data.toString();
        }
        if (strData.contains("http://aktiontrocken.com/code/")) {
            grpName = "group";
        }
        grpName = getIntent().getStringExtra("viewpagerItem");
        if (grpName == null) {
            grpName = "entry";
        }
        if (grpName.equals("i")) {
            viewPager.setCurrentItem(0);

        } else if (grpName.equals("entry")) {
            viewPager.setCurrentItem(1);

        } else if (grpName.equals("group")) {
            viewPager.setCurrentItem(2);

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        //Do whatever you want to do when the application stops.
        //Toast.makeText(getApplicationContext(), "stop",Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Do whatever you want to do when the application is destroyed.
        Toast.makeText(getApplicationContext(), "destroy", Toast.LENGTH_SHORT).show();

    }

    private void noEntriesNotify() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        int yearPos = sharedPreferences.getInt("LastEntry_Value", 0);

        DatePicker datePicker = new DatePicker(getApplicationContext());
        Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
        localCalendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
        int currentDayOfYear = localCalendar.get(Calendar.DAY_OF_YEAR);

        if (yearPos > currentDayOfYear) {
            yearPos = yearPos - 365;
        }
        int day = currentDayOfYear - yearPos;

        //Toast.makeText(getApplicationContext(), "Letzter Eintrag: " + yearPos + " Heute: " + currentDayOfYear + " Tage keine Eintrag: " + day,Toast.LENGTH_LONG).show();


    }


    @Override
    protected void onResume() {
        super.onResume();
        noEntriesNotify();

    }

    private void checkFirstRun() {
        if (prefs != null && prefs.getBoolean("firstrun", true)) {
            // Do first run stuff here then set 'firstrun' as false
            // using the following line to edit/commit prefs
            Log.d("onResume", "yeah");
            Intent myIntent = new Intent(getApplicationContext(), ProfileData.class);
            finish();
            startActivityForResult(myIntent, 0);

        }
    }

    private void setupViewPager(ViewPager viewPager) {
        Log.d("jhaksd", "hallo");

        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new IListFragment(), "News");
        adapter.addFragment(new EntryListFragment(), "Eintragen");
        adapter.addFragment(new GroupFragment(), "Gruppen");
        //adapter.addFragment(new StatsFragment(), "Statistiken");


        viewPager.setAdapter(adapter);
    }


    private void savePreferences() {
        this.prefs = getSharedPreferences("com.support.android.aktionTrocken", MODE_PRIVATE);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("url", "82.165.152.49");
        //editor.putString("url", "192.168.1.115");
        //editor.putString("url", "10.0.0.10");
        //editor.putString("port", ":3000");
        editor.putString("port", ":3005");
        editor.putString("deviceId", deviceId);
        editor.putString("userId", "");
        editor.commit();

    }
}
