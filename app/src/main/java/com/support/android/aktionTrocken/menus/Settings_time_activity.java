package com.support.android.aktionTrocken.menus;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TimePicker;

import com.support.android.aktionTrocken.R;

/**
 * Created by simon on 28.12.2015.
 */
public class Settings_time_activity extends AppCompatActivity {

    SharedPreferences.Editor editor;
    TimePicker tp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_time_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        ab.setDisplayHomeAsUpEnabled(true);

        tp = (TimePicker) findViewById(R.id.timePicker);
        tp.setIs24HourView(true);


        SharedPreferences sharedPrefs = getApplication().getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE);
        editor = sharedPrefs.edit();

        tp.setCurrentHour(Integer.valueOf(sharedPrefs.getString("tpHour", "12")));
        tp.setCurrentMinute(Integer.valueOf(sharedPrefs.getString("tpMinute", "00")));

        tp.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                editor.putString("tpHour", "" + tp.getCurrentHour());
                editor.putString("tpMinute", "" + tp.getCurrentMinute());
                editor.commit();

                //Toast.makeText(getApplicationContext(), "Hour: " + tp.getCurrentHour() + " Min: " + tp.getCurrentMinute(), Toast.LENGTH_SHORT).show();
            }
        });



    }



    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;

    }
}
