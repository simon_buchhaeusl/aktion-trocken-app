package com.support.android.aktionTrocken.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by simon on 11.08.2015.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "ProSupro.db";
    private static final int DATABASE_VERSION =1;


    private static final String TABLE_CREATE_VERLAUF = ""
            +"create table ENTRY("
            +" ID integer primary key autoincrement, "
            +	"day text, "
            +	"date text, "
            +	"detaildate text, "
            +	"consumption int, "
            +	"situation int, "
            +	"icon int, "
            +	"icon2 int,"
            +	"deviceid text)";

    public MySQLiteHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database){
        database.execSQL(TABLE_CREATE_VERLAUF);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from Version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS SCANITER");
        onCreate(db);
    }

}
