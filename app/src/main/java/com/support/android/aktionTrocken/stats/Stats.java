package com.support.android.aktionTrocken.stats;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.PercentFormatter;
import com.support.android.aktionTrocken.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Stefan on 14.08.2015.
 */
public class Stats extends AppCompatActivity {
    String deviceId, userId;
    private String url;
    private String port;
    TextView txtAlcFreeDaysDB, txtAllDaysDB, txtHomeDB, txtPartyDB, txtGoOutDB, txtMealDB, txtOtherDB;
    int drunk, noDrunk, home, party, goOut, meal, other;
    private FrameLayout mainLayout;
    private LinearLayout progressBar;
    private TextView tvDrunk, tvNotDrunk, tvNoData;
    private ProgressBar daysProgressBar, loadProgressBar;
    private PieChart mChart;
    int cGreen = Color.parseColor("#6DCD88");
    int cBlue = Color.parseColor("#FF808B");
    int cOrange = Color.parseColor("#EFD8A8");
    int cViolette = Color.parseColor("#E1A365");
    int cPink = Color.parseColor("#BD4D64");
    int cDarkRed = Color.parseColor("#722257");
    int cDarkOrange = Color.parseColor("#FF808B");
    private ArrayList<Integer> consumptionArray = new ArrayList<Integer>();
    private ArrayList<Integer> situationArray = new ArrayList<Integer>();

    // we're going to display pie chart for smartphones martket shares
    private int[] yData;
    private String[] xData;
    public HttpAsyncTaskGetFree getFree = new HttpAsyncTaskGetFree();
    public HttpAsyncTaskGetDrunk getDrunk = new HttpAsyncTaskGetDrunk();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stats_activity);

        loadPreferences();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        ab.setDisplayHomeAsUpEnabled(true);

       /* txtAlcFreeDaysDB = (TextView) findViewById(R.id.txtAlcFreeDaysDB);
        txtAllDaysDB = (TextView) findViewById(R.id.txtAllDaysDB);
        txtHomeDB = (TextView) findViewById(R.id.txtHomeDB);
        txtPartyDB = (TextView) findViewById(R.id.txtPartyDB);
        txtGoOutDB = (TextView) findViewById(R.id.txtGoOutDB);
        txtMealDB = (TextView) findViewById(R.id.txtMealDB);
        txtOtherDB = (TextView) findViewById(R.id.txtOtherDB);*/


        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = telephonyManager.getDeviceId();

        new HttpAsyncTaskGetUserId().execute("http://" + url + port + "/users/" + deviceId);

        mChart = new PieChart(this);
        progressBar = (LinearLayout) findViewById(R.id.row1include);
        daysProgressBar = (ProgressBar) findViewById(R.id.progressBarElement);
        //daysProgressBar.getProgressDrawable().setColorFilter(cGreen, PorterDuff.Mode.DST_IN);

        tvDrunk = (TextView) findViewById(R.id.tvDrunk);
        tvNotDrunk = (TextView) findViewById(R.id.tvNotDrunk);
        tvNoData = (TextView) findViewById(R.id.tvNoData);

        tvNoData.setText("Bitte eintragen");

        tvNotDrunk.setTextColor(cGreen);
        tvDrunk.setTextColor(cBlue);


        tvDrunk.setVisibility(View.GONE);
        tvNotDrunk.setVisibility(View.GONE);
        mChart.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        tvNoData.setVisibility(View.GONE);

        //makePieChart();

        //Situationen - unbedingt verbessern - DRY
        /*new HttpAsyncTaskGetHome().execute("http://" + url + port + "/stats/home/" + deviceId);
        new HttpAsyncTaskGetParty().execute("http://" + url + port + "/stats/party/" + deviceId);
        new HttpAsyncTaskGetGoOut().execute("http://" + url + port + "/stats/goOut/" + deviceId);
        new HttpAsyncTaskGetMeal().execute("http://" + url + port + "/stats/meal/" + deviceId);
        new HttpAsyncTaskGetOther().execute("http://" + url + port + "/stats/other/" + deviceId);*/
    }

    private void loadPreferences() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        url = sharedPreferences.getString("url", "");
        port = sharedPreferences.getString("port", "");
    }

    private void makePieChart() {
        daysProgressBar.setMax(noDrunk + drunk);
        daysProgressBar.setProgress(noDrunk);
        yData = new int[]{noDrunk, home, party, goOut, meal, other};
        xData = new String[]{"Verzicht", "Zuhause", "Feierlichkeit", "Ausgehen", "Essen", "Sonstige"};

        mainLayout = (FrameLayout) findViewById(R.id.layout);

        tvDrunk.setText(drunk + " getrunken");
        tvNotDrunk.setText("verzichtet " + noDrunk);

        if (!((drunk + noDrunk) == 0)) {
            tvDrunk.setVisibility(View.VISIBLE);
            tvNotDrunk.setVisibility(View.VISIBLE);
            mChart.setVisibility(View.VISIBLE);
            //daysProgressBar.setVisibility(View.VISIBLE);
        } else {
            tvNoData.setVisibility(View.VISIBLE);
        }

        // add pie chart to main layout
        mainLayout.addView(mChart);
        mainLayout.setBackgroundColor(Color.TRANSPARENT);

        // configure pie chart
        mChart.setUsePercentValues(true);
        mChart.setDescription("");

        // enable hole and configure
        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColorTransparent(true);
        mChart.setHoleRadius(60);
        mChart.setTransparentCircleRadius(10);

        // enable rotation of the chart by touch
        mChart.setRotationAngle(0);
        mChart.setRotationEnabled(false);

        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                if (e == null)
                    return;

                /*Toast.makeText(Stats.this,
                        xData[e.getXIndex()] + " = " + e.getVal() + "%", Toast.LENGTH_SHORT).show();*/
            }

            @Override
            public void onNothingSelected() {

            }
        });

        // add data
        addData();


    }

    private void addData() {
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        for (int i = 0; i < yData.length; i++)
            yVals1.add(new Entry(yData[i], i));

        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < xData.length; i++)
            xVals.add(xData[i]);

        // create pie data set
        PieDataSet dataSet = new PieDataSet(yVals1, "");
        //dataSet.setSliceSpace(3);
        dataSet.setSelectionShift(3);

        // add many colors
        ArrayList<Integer> colors = new ArrayList<Integer>();

        colors.add(cGreen);
        colors.add(cOrange);
        colors.add(cDarkOrange);
        colors.add(cPink);
        colors.add(cViolette);
        colors.add(cDarkRed);

        colors.add(ColorTemplate.getHoloBlue());
        dataSet.setColors(colors);

        // instantiate pie data object now
        PieData data = new PieData(xVals, dataSet);


        // customize legends
        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.PIECHART_CENTER);
        l.setXEntrySpace(0);
        l.setYEntrySpace(0);

        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.TRANSPARENT);
        mChart.setData(data);

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);


        // undo all highlights
        mChart.highlightValues(null);

        // update pie chart
        mChart.invalidate();
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        finish();
        return true;
    }

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private class HttpAsyncTaskGetFree extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            //txtAlcFreeDaysDB.setText(result + "");
            noDrunk = Integer.parseInt(result);
        }
    }

    private class HttpAsyncTaskGetDrunk extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            //txtAllDaysDB.setText(result + "");
            drunk = Integer.parseInt(result);
        }
    }

    private class HttpAsyncTaskGetHome extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            home = Integer.parseInt(result);
        }
    }

    private class HttpAsyncTaskGetParty extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            party = Integer.parseInt(result);
        }
    }

    private class HttpAsyncTaskGetGoOut extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            goOut = Integer.parseInt(result);
        }
    }

    private class HttpAsyncTaskGetMeal extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            meal = Integer.parseInt(result);
        }
    }

    private class HttpAsyncTaskGetOther extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            other = Integer.parseInt(result);
            makePieChart();
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public class HttpAsyncTaskGetUserId extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONArray json = new JSONArray(result);

                JSONObject jsonobject = json.getJSONObject(0);

                userId = jsonobject.getString("_id");

            } catch (JSONException e) {

                throw new RuntimeException(e);
            }

            new HttpAsyncTaskGetEntries().execute("http://" + url + port + "/entries/" + userId);
            //getFree.execute("http://" + url + port + "/stats/alcFreeDays/" + deviceId);
            //getDrunk.execute("http://" + url + port + "/stats/alcDays/" + deviceId);
        }
    }

    public class HttpAsyncTaskGetEntries extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONArray json = new JSONArray(result);

                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonobject = json.getJSONObject(i);

                    consumptionArray.add(jsonobject.getInt("consumption"));
                    situationArray.add(jsonobject.getInt("situation"));
                }

                noDrunk = Collections.frequency(consumptionArray, 0);
                drunk = Collections.frequency(consumptionArray, 1);
                home = Collections.frequency(situationArray, 1);
                party = Collections.frequency(situationArray, 2);
                goOut = Collections.frequency(situationArray, 3);
                meal = Collections.frequency(situationArray, 4);
                other = Collections.frequency(situationArray, 5);
                makePieChart();

            } catch (JSONException e) {

                throw new RuntimeException(e);
            }
        }
    }
}
