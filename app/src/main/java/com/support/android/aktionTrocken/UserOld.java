package com.support.android.aktionTrocken;

/**
 * Created by Stefan on 12.08.2015.
 */
public class UserOld {
    private int userId;
    private String deviceId, year, sex, country, state;;

    public UserOld(int userId, String sex, String country, String state, String deviceId, String year) {
        this.userId = userId;
        this.sex = sex;
        this.country = country;
        this.state = state;
        this.deviceId = deviceId;
        this.year = year;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
