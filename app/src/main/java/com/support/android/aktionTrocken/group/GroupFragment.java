package com.support.android.aktionTrocken.group;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
import com.support.android.aktionTrocken.R;
import com.support.android.aktionTrocken.menus.Settings_activity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class GroupFragment extends Fragment {
    private ListView listViewG;
    private GroupAdapter mAdapter;
    private Group group;
    private String deviceId;
    private String url;
    private String port, userId;
    private ArrayList<Integer> alcFreeDaysArrayNew = new ArrayList<Integer>();
    private ArrayList<String> groupId = new ArrayList<String>();
    private ArrayList<String> groupName = new ArrayList<String>();
    private ArrayList<Integer> memCreationDate = new ArrayList<Integer>();
    private ArrayList<Integer> groupMember = new ArrayList<Integer>();
    private ArrayList<Integer> groupDays = new ArrayList<Integer>();
    private ArrayList<Integer> demFeature = new ArrayList<Integer>();
    private ArrayList<Integer> typeArray = new ArrayList<Integer>();
    private ArrayList<Integer> groupDate = new ArrayList<Integer>();
    private ArrayList<String> groupCode = new ArrayList<String>();
    private ArrayList<Integer> counterArray = new ArrayList<Integer>();
    private String grpName, detailDate, grpCode;
    private int grpMember, grpAlcFreeDays, grpDate, cntForGetMem = 0, cntForGetGroup = 0, cntForGetDays = 0, joinDateMem = 0, demographicFeature, type;
    private ProgressBar pb;
    private Boolean progrBbool = false;
    private String grpNameForMem, groupIdForMem, searchedCode;

    public GroupFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = telephonyManager.getDeviceId();
        loadPreferences();


        final ArrayList<Group> groupArrayList = new ArrayList<Group>();
        mAdapter = new GroupAdapter(getActivity(), groupArrayList);
    }

    private void loadPreferences() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        url = sharedPreferences.getString("url", "");
        port = sharedPreferences.getString("port", "");
        new HttpAsyncTaskGetUserId().execute("http://" + url + port + "/users/" + deviceId);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.group_list_fragment, container, false);

        listViewG = (ListView) rootView.findViewById(R.id.listViewGroup);
        listViewG.setAdapter(mAdapter);

        pb = (ProgressBar) rootView.findViewById(R.id.progressBar2);
        if (progrBbool == true) {
            pb.setVisibility(View.GONE);
        }


        listViewG.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "pos " + position, Toast.LENGTH_SHORT).show();
                grpName = groupName.get(position);
                grpMember = groupMember.get(position);
                grpAlcFreeDays = groupDays.get(position);
                grpDate = groupDate.get(position);
                grpCode = groupCode.get(position);
                joinDateMem = memCreationDate.get(position);
                demographicFeature = demFeature.get(position);
                type = typeArray.get(position);

                Intent i = new Intent(getActivity(), Group_activity.class);
                i.putExtra("grpName", "" + grpName);
                i.putExtra("grpMember", "" + grpMember);
                i.putExtra("grpAlcFreeDays", "" + grpAlcFreeDays);
                i.putExtra("creationDate", "" + grpDate);
                i.putExtra("code", "" + grpCode);
                i.putExtra("joinDate", "" + joinDateMem);
                i.putExtra("userId", userId);
                i.putExtra("demographicFeature", demographicFeature);
                i.putExtra("type", type);
                startActivity(i);
            }
        });

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_group, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_settings:
                Intent i2 = new Intent(getActivity(), Settings_activity.class);
                startActivity(i2);
                Toast.makeText(getActivity(), "einstellungen", Toast.LENGTH_SHORT).show();

                return true;
            case R.id.menu_item_newGroup:
                showSimpleList2(getView());

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void showInputDialog() {
        new MaterialDialog.Builder(getActivity())
                .title("Beitrittscode eingeben")
                .inputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME |
                        InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                .inputMaxLength(5)
                .backgroundColor(Color.LTGRAY)
                .titleColor(Color.BLACK)
                .contentColor(Color.BLACK)
                .positiveText("Beitreten")
                .positiveColor(Color.BLACK)
                .input("", "", false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {

                        ViewGroup vg = (ViewGroup) getView().findViewById(R.id.mainLayout);
                        vg.invalidate();

                        try {
                            searchedCode = String.valueOf(input);
                        } catch (NumberFormatException e) {
                            Toast.makeText(getActivity(), "Korrekter Code eingeben!", Toast.LENGTH_SHORT).show();
                        }
                        new HttpAsyncTaskSearch().execute("http://" + url + port + "/groups/code/" + searchedCode);

                    }
                }).show();
    }

    public void showSimpleList2(final View v) {
        final MaterialSimpleListAdapter adapter = new MaterialSimpleListAdapter(v.getContext());
        adapter.add(new MaterialSimpleListItem.Builder(v.getContext())
                .content("Gruppe erstellen")
                .build());
        adapter.add(new MaterialSimpleListItem.Builder(v.getContext())
                .content("Gruppe beitreten")
                .build());
        new MaterialDialog.Builder(v.getContext())
                .backgroundColor(Color.WHITE)
                .titleColor(Color.BLACK)
                .contentColor(Color.BLACK)
                .adapter(adapter, new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        MaterialSimpleListItem item = adapter.getItem(which);
                        Log.d("yo", "popup" + item.getContent().toString());
                        String s = item.getContent().toString();
                        if (s.equals("Gruppe erstellen")) {
                            Intent i3 = new Intent(getActivity(), AddGroup_activity.class);
                            startActivity(i3);

                        } else if (s.equals("Gruppe beitreten")) {
                            showInputDialog();

                        }
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private String getDetailDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy", Locale.GERMAN);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        detailDate = dateFormat.format(cal.getTime());
        //detailDate = detailDate.replace(".", "");
        Log.d("", "" + detailDate);

        return detailDate;
    }

    private class HttpAsyncTaskSearch extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONArray json = new JSONArray(result);

                JSONObject jsonobject = json.getJSONObject(0);

                grpNameForMem = jsonobject.getString("name");
                groupIdForMem = jsonobject.getString("_id");
                new HttpAsyncTaskPostMem().execute("http://" + url + port + "/memberships/");

            } catch (JSONException e) {
                Toast.makeText(getActivity(), "Korrekter Code eingeben!", Toast.LENGTH_SHORT).show();
                showInputDialog();
            }
        }
    }

    private class HttpAsyncTaskPostMem extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return POSTMem(urls[0]);

        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Log.d("data send", "");
        }
    }

    public String POSTMem(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

            String json = "";
            String deviceId = this.deviceId;
            String userId = this.userId;
            getDetailDate();
            String joinDate = detailDate;
            String groupName = grpName;

            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();

            //*jsonObject.accumulate("deviceId", this.deviceId);
            jsonObject.accumulate("userId", userId);
            jsonObject.accumulate("joinDate", joinDate);
            jsonObject.accumulate("deviceId", deviceId);
            jsonObject.accumulate("groupName", grpNameForMem);
            jsonObject.accumulate("groupId", groupIdForMem);

            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();

            // ** Alternative way to convert Person object to JSON string usin Jackson Lib
            // ObjectMapper mapper = new ObjectMapper();
            // json = mapper.writeValueAsString(person);

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        // 11. return result
        return result;
    }

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public class HttpAsyncTaskGetUserId extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONArray json = new JSONArray(result);

                JSONObject jsonobject = json.getJSONObject(0);

                userId = jsonobject.getString("_id");

                new HttpAsyncTaskGetMem().execute("http://" + url + port + "/memberships/active/" + userId);

            } catch (JSONException e) {

                throw new RuntimeException(e);
            }
        }
    }

    private class HttpAsyncTaskGetMem extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            int k;
            try {
                JSONArray json = new JSONArray(result);

                for (k = 0; k < json.length(); k++) {
                    JSONObject jsonobject = json.getJSONObject(k);
                    groupId.add(jsonobject.getString("groupId"));
                    memCreationDate.add(jsonobject.getInt("joinDate"));
                    counter(k);
                }

                for (int j = 0; j < counterArray.size(); j++) {
                    new HttpAsyncTaskGetGroup().execute("http://" + url + port + "/groups/groupId/" + groupId.get(counterArray.get(j)));
                }

            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void counter(int j) {
        counterArray.add(j);
    }

    private class HttpAsyncTaskGetGroup extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                JSONArray json = new JSONArray(result);

                JSONObject jsonobject = json.getJSONObject(0);
                groupName.add(jsonobject.getString("name"));
                int creationDate = jsonobject.getInt("creationDate");
                groupDate.add(creationDate);
                groupCode.add(jsonobject.getString("code"));
                demFeature.add(jsonobject.getInt("demographicFeature"));
                typeArray.add(jsonobject.getInt("type"));

                new HttpAsyncTaskGetGroupDays().execute("http://" + url + port + "/entries/count/byGroup/" + groupId.get(cntForGetGroup) + "?fromDate=" + creationDate + "&toDate=" + getDetailDate() + "&state=0");
                new HttpAsyncTaskGetGroupMem().execute("http://" + url + port + "/users/active/count/" + groupId.get(cntForGetGroup));
                cntForGetGroup++;


            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private class HttpAsyncTaskGetGroupMem extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            int realResult = Integer.parseInt(result);
            groupMember.add(realResult);
            group = new Group(1, R.drawable.ic_group_48pt_2x, groupName.get(cntForGetMem), realResult, alcFreeDaysArrayNew.get(cntForGetDays), 412563);
            cntForGetDays++;
            cntForGetMem++;
            mAdapter.add(group);
            pb.setVisibility(View.GONE);
            progrBbool = true;

        }
    }

    private class HttpAsyncTaskGetGroupDays extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            int realResult = Integer.parseInt(result);
            alcFreeDaysArrayNew.add(realResult);
            groupDays.add(realResult);
        }
    }
}
