package com.support.android.aktionTrocken.group;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.support.android.aktionTrocken.MainActivity;
import com.support.android.aktionTrocken.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by simon on 13.08.2015.
 */
public class AddGroup_activity extends AppCompatActivity implements IconChooserDialog.Callback{
    private int id;
    private int grpIcon;
    private String grpName, deviceId, detailDate, userId, type = "2";
    private int grpMember;
    private int grpAlcFreeDay;
    private String grpShareCode, groupIdForMem, grpNameForMem;
    private String url;
    private String port;
    private Intent intent = new Intent(Intent.ACTION_SEND);
    private ArrayList<String> groupId = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addgroup_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TelephonyManager telephonyManager = (TelephonyManager) getApplication().getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = telephonyManager.getDeviceId();

        loadPreferences();

        new HttpAsyncTaskGetUserId().execute("http://" + url + port + "/users/" + deviceId);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        ab.setDisplayHomeAsUpEnabled(true);

        //ImageButton grpIconBtn = (ImageButton) findViewById(R.id.grpIconBtn);
        final EditText grpNameText = (EditText) findViewById(R.id.grpNameText);
        Button grpCreate = (Button) findViewById(R.id.grpCreate);

        /*grpIconBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplication(),"On icon btn", Toast.LENGTH_SHORT).show();
                showCustomColorChooser();
            }
        });*/
        grpCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grpName = grpNameText.getText().toString();

                if (grpName.equals("")) {
                    Toast.makeText(getApplication(), "Tragen sie einen Namen ein", Toast.LENGTH_SHORT).show();
                } else {
                    showInputDialog2();
                }
            }

        });
    }

    private void loadPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplication());
        url = sharedPreferences.getString("url", "");
        port = sharedPreferences.getString("port", "");
    }

    private String getDetailDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy", Locale.GERMAN);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        detailDate = dateFormat.format(cal.getTime());
        //detailDate = detailDate.replace(".", "");
        Log.d("", "" + detailDate);

        return detailDate;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        finish();
        return true;

    }

    public void onColorSelection(int index, String color, int darker) {
        //ImageButton grpIconBtn = (ImageButton) findViewById(R.id.grpIconBtn);
        int resID = getResources().getIdentifier(color , "drawable", getPackageName());
        //grpIconBtn.setImageResource(resID);
    }



    private void showInputDialog2() {
        new MaterialDialog.Builder(this)
                .title("Beitrittscode teilen")
                .content("Gruppe \"" + grpName + "\"")
                .backgroundColor(Color.WHITE)
                .titleColor(Color.BLACK)
                .contentColor(Color.DKGRAY)
                .positiveText("Teilen").positiveColor(getResources().getColor(R.color.agree))
                .negativeText("‹berspringen").negativeColor(getResources().getColor(R.color.agree))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Toast.makeText(getApplicationContext(), "Teilen ", Toast.LENGTH_SHORT).show();
                        intent.setType("text/plain");
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        new HttpAsyncTaskAddGroup().execute("http://" + url + port + "/groups/");
                        Toast.makeText(getApplicationContext(), "Create Group: " + grpName, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AddGroup_activity.this, MainActivity.class);
                        intent.putExtra("viewpagerItem", "group");
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .show();
    }

    private class HttpAsyncTaskAddGroup extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            groupIdForMem = null;
            try {

                JSONObject jsonobject = new JSONObject(result);
                groupIdForMem = jsonobject.getString("_id");
                grpNameForMem = jsonobject.getString("name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            new HttpAsyncTaskPostMem().execute("http://" + url + port + "/memberships/");
        }
    }

    public String POST(String url){
        InputStream inputStream = null;
        String result = "";
        try {

            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

            String json = "";

            getDetailDate();
            grpIcon = 2;
            grpMember = 1;
            grpAlcFreeDay = 1;
            grpShareCode = "1";

            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("image", grpIcon);
            jsonObject.accumulate("name", grpName);
            jsonObject.accumulate("type", type);
            jsonObject.accumulate("creationDate", detailDate);
            jsonObject.accumulate("demographicFeature", "1");

            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();

            // ** Alternative way to convert Person object to JSON string usin Jackson Lib
            // ObjectMapper mapper = new ObjectMapper();
            // json = mapper.writeValueAsString(person);

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        // 11. return result
        return result;
    }
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    public class HttpAsyncTaskGetUserId extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONArray json = new JSONArray(result);

                JSONObject jsonobject = json.getJSONObject(0);

                userId = jsonobject.getString("_id");

            } catch (JSONException e) {

                throw new RuntimeException(e);
            }
        }
    }

    public String POSTMem(String url){
        InputStream inputStream = null;
        String result = "";
        try {

            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

            String json = "";
            String deviceId = this.deviceId;
            String userId = this.userId;
            String joinDate = detailDate;

            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();

            //*jsonObject.accumulate("deviceId", this.deviceId);
            jsonObject.accumulate("userId", userId);
            jsonObject.accumulate("joinDate", joinDate);
            jsonObject.accumulate("deviceId", deviceId);
            jsonObject.accumulate("groupName", grpNameForMem);
            jsonObject.accumulate("groupId", groupIdForMem);

            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();

            // ** Alternative way to convert Person object to JSON string usin Jackson Lib
            // ObjectMapper mapper = new ObjectMapper();
            // json = mapper.writeValueAsString(person);

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        // 11. return result
        return result;
    }

    private class HttpAsyncTaskPostMem extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return POSTMem(urls[0]);

        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Log.d("data send", "");

        }

    }
}
