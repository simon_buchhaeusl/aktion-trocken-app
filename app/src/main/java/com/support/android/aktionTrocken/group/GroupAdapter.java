package com.support.android.aktionTrocken.group;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.support.android.aktionTrocken.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by simon on 14.08.2015.
 */
public class GroupAdapter extends ArrayAdapter<Group> {
    private Group[] groups;

    public GroupAdapter(Context context, ArrayList<Group> groups) {
        super(context, 0, groups);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Group group = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_group, parent, false);
        }
        // Lookup view for data population
        TextView grpName = (TextView) convertView.findViewById(R.id.grpName);
        TextView grpMember = (TextView) convertView.findViewById(R.id.grpMember);
        TextView grpAlcFreeDay = (TextView) convertView.findViewById(R.id.grpAlcFreeDay);
        CircleImageView grpIcon = (CircleImageView) convertView.findViewById(R.id.grpIcon);

        // Populate the data into the template view using the data object
        grpName.setText(group.getGrpName());
        grpMember.setText(group.getGrpMember() + "");
        grpAlcFreeDay.setText("alkoholfreie Tage: " + group.getGrpAlcFreeDay());
        grpIcon.setImageResource(group.getGrpIcon());

        // Return the completed view to render on screen

        return convertView;
    }
}
