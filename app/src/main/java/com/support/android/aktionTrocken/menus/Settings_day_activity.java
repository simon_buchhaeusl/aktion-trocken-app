package com.support.android.aktionTrocken.menus;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.support.android.aktionTrocken.R;

import java.util.ArrayList;

/**
 * Created by simon on 14.12.2015.
 */
public class Settings_day_activity extends AppCompatActivity {

    private MyCustomAdapter dataAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_day_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        ab.setDisplayHomeAsUpEnabled(true);
        displayListView();
    }

    private void displayListView() {

        ArrayList<Day> dayslist = new ArrayList<Day>();
        Day day1 = new Day("", "Montag", false);
        dayslist.add(day1);
        Day day2 = new Day("", "Dienstag", false);
        dayslist.add(day2);
        Day day3 = new Day("", "Mittwoch", false);
        dayslist.add(day3);
        Day day4 = new Day("", "Donnerstag", false);
        dayslist.add(day4);
        Day day5 = new Day("", "Freitag", false);
        dayslist.add(day5);
        Day day6 = new Day("", "Samstag", false);
        dayslist.add(day6);
        Day day7 = new Day("", "Sonntag", false);
        dayslist.add(day7);

        //create an ArrayAdaptar from the String Array
        dataAdapter = new MyCustomAdapter(this,
                R.layout.settings_day_row, dayslist);
        ListView listView = (ListView) findViewById(R.id.listView1);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                Day day = (Day) parent.getItemAtPosition(position);
                //Toast.makeText(getApplicationContext(),
                 //       "Clicked on Row: " + day.getName(),
                 //       Toast.LENGTH_LONG).show();
            }
        });

    }

    private class MyCustomAdapter extends ArrayAdapter<Day> {

        private ArrayList<Day> dayList;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<Day> dayList) {
            super(context, textViewResourceId, dayList);
            this.dayList = new ArrayList<Day>();
            this.dayList.addAll(dayList);
        }

        private class ViewHolder {
            TextView code;
            CheckBox name;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            SharedPreferences sharedPrefs = getContext().getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE);
            final SharedPreferences.Editor editor;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.settings_day_row, null);

                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(holder);

                holder.name.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v;
                        Day day = (Day) cb.getTag();
                        //Toast.makeText(getApplicationContext(), "Clicked on Checkbox: " + cb.getText() + " is " + cb.isChecked(),Toast.LENGTH_LONG).show();
                        day.setSelected(cb.isChecked());
                    }
                });
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Day day = dayList.get(position);
            holder.code.setText("");
            holder.name.setText(day.getName());
            holder.name.setChecked(day.isSelected());
            holder.name.setTag(day);

            editor = sharedPrefs.edit();

            holder.name.setChecked(sharedPrefs.getBoolean("CheckValue" + position, false));
            //Toast.makeText(getContext(), "Value " + position + "|" + sharedPrefs.getBoolean("CheckValue" + position, false), Toast.LENGTH_SHORT).show();
            holder.name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    editor.putBoolean("CheckValue" + position, true);
                    editor.commit();
                }
            });

            return convertView;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;

    }

}


