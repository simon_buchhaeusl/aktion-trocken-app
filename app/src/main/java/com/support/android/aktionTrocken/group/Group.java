package com.support.android.aktionTrocken.group;

/**
 * Created by simon on 14.08.2015.
 */
public class Group {
    private int id;
    private int grpIcon;
    private String grpName;
    private int grpMember;
    private int grpAlcFreeDay;
    private int grpShareCode;

    public Group(int id, int grpIcon, String grpName, int grpMember, int grpAlcFreeDay, int grpShareCode) {
        this.id = id;
        this.grpIcon = grpIcon;
        this.grpName = grpName;
        this.grpMember = grpMember;
        this.grpAlcFreeDay = grpAlcFreeDay;
        this.grpShareCode = grpShareCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGrpIcon() {
        return grpIcon;
    }

    public void setGrpIcon(int grpIcon) {
        this.grpIcon = grpIcon;
    }

    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    public int getGrpMember() {
        return grpMember;
    }

    public void setGrpMember(int grpMember) {
        this.grpMember = grpMember;
    }

    public int getGrpAlcFreeDay() {
        return grpAlcFreeDay;
    }

    public void setGrpAlcFreeDay(int grpAlcFreeDay) {
        this.grpAlcFreeDay = grpAlcFreeDay;
    }

    public int getGrpShareCode() {
        return grpShareCode;
    }

    public void setGrpShareCode(int grpShareCode) {
        this.grpShareCode = grpShareCode;
    }
}
