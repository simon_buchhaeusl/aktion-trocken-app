package com.support.android.aktionTrocken.group;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.LargeValueFormatter;
import com.support.android.aktionTrocken.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by simon on 14.08.2015.
 */
public class Group_activity extends AppCompatActivity implements View.OnClickListener {
    private String strData = "", url, port;
    private String grpName, deviceId, grpCode;
    private int grpMember, grpAlcFreeDays, grpAverageDays, grpDate, memDate, grpDays, memDays, noDrunk, demographicFeature, type;
    private TextView txtMember, txtAlcFreeDays, txtAverageDays, txtJoinDate, txtOwnDays, txtShare;
    private ImageView imgOwnDays;
    private ImageButton imgbtnShare;
    private Intent intent = new Intent(Intent.ACTION_SEND);
    private BarChart mChart;
    private String detailDatePost;
    private String convertedDate;
    private String nowDate, userId;
    private Date fromDate, toDate, joinDateGrp, joinDateMem, todayDate;
    private ArrayList<Integer> consumptionArray = new ArrayList<Integer>();
    private float grpAverage, memAverage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadPreferences();

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = telephonyManager.getDeviceId();

        txtMember = (TextView) findViewById(R.id.txtMember);
        txtAlcFreeDays = (TextView) findViewById(R.id.txtAlcFreeDays);
        txtAverageDays = (TextView) findViewById(R.id.txtAverageDays);
        txtJoinDate = (TextView) findViewById(R.id.txtJoinDate);
        txtOwnDays = (TextView) findViewById(R.id.txtOwnDays);
        txtShare = (TextView) findViewById(R.id.txtShare);

        imgOwnDays = (ImageView) findViewById(R.id.imgOwnDays);

        imgbtnShare = (ImageButton) findViewById(R.id.imgbtnShare);

        imgOwnDays.setVisibility(View.GONE);

        grpName = getIntent().getStringExtra("grpName");
        grpMember = Integer.parseInt(getIntent().getStringExtra("grpMember"));
        grpAlcFreeDays = Integer.parseInt(getIntent().getStringExtra("grpAlcFreeDays"));
        grpDate = Integer.parseInt(getIntent().getStringExtra("creationDate"));
        grpCode = getIntent().getStringExtra("code");
        memDate = Integer.parseInt(getIntent().getStringExtra("joinDate"));
        demographicFeature = getIntent().getIntExtra("demographicFeature", 0);
        userId = getIntent().getStringExtra("userId");
        type = getIntent().getIntExtra("type", 0);

        grpAverageDays = grpAlcFreeDays / grpMember;
        new HttpAsyncTaskGetEntries().execute("http://" + url + port + "/entries/" + userId);
        try {
            convertGroupDate(grpDate);
            convertMemDate(memDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (type == 1) {
            txtShare.setVisibility(View.GONE);
            imgbtnShare.setVisibility(View.GONE);
        }

        getDetailDate();
        DateFormat format = new SimpleDateFormat("dd.MM.yy", Locale.ENGLISH);
        try {
            todayDate = format.parse(nowDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d("", nowDate);
        Log.d("", convertedDate);
        Log.d("",countWeeks());

        grpDays = countDays(joinDateGrp, todayDate);
        memDays = countDays(joinDateMem, todayDate);
        grpAverage = (100 * grpAverageDays) / grpAlcFreeDays;

        String grpMemberTxt;
        if(grpMember > 1){
            grpMemberTxt = "Mitglieder";
        } else{
            grpMemberTxt = "Mitglied";
        }

        String grpAlcFreeDaysTxt;
        if(grpAlcFreeDays > 1){
            grpAlcFreeDaysTxt = "Verzichtstage";
        } else{
            grpAlcFreeDaysTxt = "Verzichtstag";
        }

        txtMember.setText("" + grpMember + " " + grpMemberTxt);
        txtAlcFreeDays.setText("" + grpAlcFreeDays + " " + grpAlcFreeDaysTxt);
        //txtAverageDays.setText("" + grpAverageDays);
        //txtAverageDays.setText("Alle hier dargestellten Daten sind nur Testdaten. Die Implementierung dieses Features l�uft und wird demn�chst in Form eines Updates zur Verf�gung stehen!");
        txtAverageDays.setText("");

        imgbtnShare.setOnClickListener(this);

        Uri data = getIntent().getData();
        if (data != null) {
            strData = data.toString();
        }

        if (strData.contains("http://aktiontrocken.com/code/")) {
            String grpCodeData = strData.substring(strData.lastIndexOf("/") + 1);
            Toast.makeText(getApplicationContext(), "Code " + grpCodeData, Toast.LENGTH_LONG).show();
            grpName = "Name" + grpCodeData;
        }

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        ab.setDisplayHomeAsUpEnabled(true);

        ab.setTitle("Gruppen");
        TextView grpNameTxt = (TextView) findViewById(R.id.grpNameTxt);
        grpNameTxt.setText("" + grpName);
    }

    private void loadPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplication());
        url = sharedPreferences.getString("url", "");
        port = sharedPreferences.getString("port", "");
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgbtnShare:
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "Hallo, willst du meiner Gruppe \"" + grpName + "\" beitreten? Hier ist der Beitrittscode: " + grpCode + ".");
                startActivity(Intent.createChooser(intent, "Gruppe teilen"));
                break;
        }
    }

    public class HttpAsyncTaskGetEntries extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONArray json = new JSONArray(result);

                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonobject = json.getJSONObject(i);

                    consumptionArray.add(jsonobject.getInt("consumption"));
                }

                noDrunk = Collections.frequency(consumptionArray, 0);

                memAverage = (noDrunk*100)/memDays;

                mChart = (BarChart) findViewById(R.id.chart1);
                mChart.setDescription("");

                Legend l = mChart.getLegend();
                l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
                l.setYOffset(0f);
                l.setYEntrySpace(0f);
                l.setTextSize(8f);

                XAxis xl = mChart.getXAxis();

                YAxis leftAxis = mChart.getAxisLeft();
                leftAxis.setValueFormatter(new LargeValueFormatter());
                leftAxis.setDrawGridLines(false);
                leftAxis.setSpaceTop(30f);

                mChart.getAxisRight().setEnabled(false);

                //////

                ArrayList<String> xVals = new ArrayList<String>();
                xVals.add("Gesamt�bersicht");

                ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
                ArrayList<BarEntry> yVals2 = new ArrayList<BarEntry>();

                float mult = 10;

                yVals1.add(new BarEntry(grpAverage, 0));
                yVals2.add(new BarEntry(memAverage, 0));

                // create 3 datasets with different types
                BarDataSet set1 = new BarDataSet(yVals1, "Diese Gruppe");
                // set1.setColors(ColorTemplate.createColors(getApplicationContext(),
                // ColorTemplate.FRESH_COLORS));
                set1.setColor(Color.parseColor("#722257"));
                BarDataSet set2 = new BarDataSet(yVals2, "Du");
                set2.setColor(Color.parseColor("#6dcdb8"));


                ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
                dataSets.add(set1);
                dataSets.add(set2);

                BarData data1 = new BarData(xVals, dataSets);
//        data.setValueFormatter(new LargeValueFormatter());

                // add space between the dataset groups in percent of bar-width
                data1.setGroupSpace(40f);

                mChart.setData(data1);

                mChart.invalidate();

                mChart.animateY(3000);


            } catch (JSONException e) {

                throw new RuntimeException(e);
            }
        }

    }

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    private String getDetailDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy", Locale.GERMAN);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        detailDatePost = dateFormat.format(cal.getTime());
        //detailDatePost = detailDatePost.replace(".", "");
        Log.d("", "" + detailDatePost);
        nowDate = detailDatePost;
        return nowDate;
    }

    private Date convertGroupDate(int date) throws ParseException {
        String dateString = String.valueOf(date);

        char pos1 = dateString.charAt(4);
        String c = String.valueOf(pos1);
        char pos2 = dateString.charAt(5);
        String c2 = String.valueOf(pos2);
        String check = "." + dateString.charAt(2) + dateString.charAt(3) + "." + dateString.charAt(0) + dateString.charAt(1);
        convertedDate = "" + pos1 + pos2;
        convertedDate = "" + convertedDate + check;

        DateFormat format = new SimpleDateFormat("dd.MM.yy", Locale.GERMAN);
        joinDateGrp = format.parse(convertedDate);

        return joinDateGrp;
    }

    private Date convertMemDate(int date) throws ParseException {
        String dateString = String.valueOf(date);

        char pos1 = dateString.charAt(4);
        String c = String.valueOf(pos1);
        char pos2 = dateString.charAt(5);
        String c2 = String.valueOf(pos2);
        String check = "." + dateString.charAt(2) + dateString.charAt(3) + "." + dateString.charAt(0) + dateString.charAt(1);
        convertedDate = "" + pos1 + pos2;
        convertedDate = "" + convertedDate + check;

        DateFormat format = new SimpleDateFormat("dd.MM.yy", Locale.GERMAN);
        joinDateMem = format.parse(convertedDate);

        return joinDateMem;
    }

    private String countWeeks() {
        DateFormat format = new SimpleDateFormat("dd.MM.yy", Locale.GERMAN);
        try {
            fromDate = format.parse(convertedDate);
            toDate = format.parse(nowDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long timeOne = fromDate.getTime();
        long timeTwo = toDate.getTime();
        long oneDay = 1000 * 60 * 60 * 24;
        long delta = (timeTwo - timeOne) / oneDay;

        if (delta > 0) {
            return "dateTwo is " + delta + " days after dateOne";
        }
        else {
            delta *= -1;
            return "dateTwo is " + delta + " days before dateOne";
        }
    }

    public int countDays (Date d1, Date d2) {
        Calendar cal1 = new GregorianCalendar();
        Calendar cal2 = new GregorianCalendar();

        cal1.setTime(d1);
        cal2.setTime(d2);

        //cal1.set(2008, 8, 1);
        //cal2.set(2008, 9, 31);
        System.out.println("Days= "+daysBetween(cal1.getTime(),cal2.getTime()));

        int days = daysBetween(cal1.getTime(),cal2.getTime()) + 1;

        return days;
    }

    public int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }
}
