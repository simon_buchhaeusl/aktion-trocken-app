package com.support.android.aktionTrocken.entry;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.support.android.aktionTrocken.R;
import com.support.android.aktionTrocken.menus.Settings_activity;
import com.support.android.aktionTrocken.stats.Stats;


public class EntryListFragment extends Fragment {

    private ListView listView;
    private String deviceId, userId;
    private String url;
    private String port;
    private EntryAdapter mAdapter;

    // remember fields
    private ProgressBar pBar;
    private ListView lvEntris;

    private boolean countdown;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mAdapter = new EntryAdapter(getActivity(), this);

        loadPreferences();
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = telephonyManager.getDeviceId();

    }

    private void loadPreferences() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        this.url = sharedPreferences.getString("url", "");
        this.port = sharedPreferences.getString("port", "");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        listView = (ListView) view.findViewById(R.id.listViewEntries);
        listView.setAdapter(this.mAdapter);
  }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.entry_list_fragment, container, false);

        this.pBar = (ProgressBar) rootView.findViewById(R.id.progBarEntry);
        this.lvEntris = (ListView) rootView.findViewById(R.id.listViewEntries);

        showProgressDialog();

        return rootView;
    }

    public void showProgressDialog() {
        this.pBar.setVisibility(View.VISIBLE);
        this.lvEntris.setVisibility(View.GONE);
    }


    public void hideProgressDialog(){
        this.pBar.setVisibility(View.INVISIBLE);
        this.lvEntris.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_entry, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_test:
                //Toast.makeText(getActivity(),"profil",Toast.LENGTH_SHORT).show();
                Intent i1 = new Intent(getActivity(), Stats.class);
                startActivity(i1);

                return true;
            case R.id.menu_settings:
                Intent i2 = new Intent(getActivity(), Settings_activity.class);

                startActivity(i2);

                //Toast.makeText(getActivity(),"einstellungen",Toast.LENGTH_SHORT).show();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
