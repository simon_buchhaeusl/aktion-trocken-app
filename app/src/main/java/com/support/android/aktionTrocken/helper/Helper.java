package com.support.android.aktionTrocken.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.support.android.aktionTrocken.entry.Entry;
import com.support.android.aktionTrocken.exception.RestException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by simon on 19.01.2016.
 */
public class Helper {
    public static String getDetailDate(int i) {
        String detailDate;
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy", Locale.GERMAN);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -i);
        detailDate = dateFormat.format(cal.getTime());

        return detailDate;
    }

    public static String getActualDateMinusDays(int days) {
        String date;
        DateFormat dateFormat = new SimpleDateFormat("dd MMM", Locale.GERMAN);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -days);
        date = dateFormat.format(cal.getTime());

        return date;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public static String getUserId(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null) {
                result = Helper.convertInputStreamToString(inputStream);
                JSONArray json = new JSONArray(result);
                JSONObject jsonobject = json.getJSONObject(0);
                result = jsonobject.getString("_id");

            } else
                result = "Did not work!";

        } catch (
                Exception e
                )

        {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    public static String updateEntryForUser(String url, Entry entry, String userId) {
        InputStream inputStream = null;
        String result = "";
        try {
            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

            String json = "";
            int consumption = entry.getConsumption();
            String date = entry.getDetailDate();

            int situation = entry.getSituation();
            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("userId", userId);
            jsonObject.accumulate("consumption", String.valueOf(consumption));
            jsonObject.accumulate("date", date);
            jsonObject.accumulate("situation", String.valueOf(situation));

            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();
            Log.d("Aktin trocken", "Sending data:" + json);

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

            // setSituationImage(i);
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        // 11. return result
        return result;
    }

    public static String getJSONFromRestCall(String url) throws RestException{
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                throw new RestException("result is empty");

        } catch (Exception e) {
           throw new RestException(e.getMessage());
        }

        return result;
    }

}
