package com.support.android.aktionTrocken;

/**
 * Created by Stefan on 12.08.2015.
 */
public class User {
    private String deviceId;
    private String joinDate;

    public User(String deviceId, String joinDate) {
        this.deviceId = deviceId;
        this.joinDate = joinDate;
    }

    public String getUserId() {
        return deviceId;
    }

    public void setUserId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }
}
