package com.support.android.aktionTrocken;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by simon on 06.08.2015.
 */
public class ProfileData extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private SharedPreferences prefs = null;
    private CheckBox cbMale, cbFemale;
    private Spinner spYear, spCountry, spState;
    private EditText etYear;
    private User userData;
    private String deviceId, joinDate, userId;
    private String url;
    private String port;
    private int countForPost= 0;
    private ArrayList<String> demoFeature = new ArrayList<String>();
    private ArrayList<String> demoGroups = new ArrayList<String>();
    private ArrayList<String> groupId = new ArrayList<String>();
    private ArrayAdapter<CharSequence> aCountry, aState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profil_data_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadPreferences();

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        ab.setDisplayHomeAsUpEnabled(false);

        prefs = getSharedPreferences("com.support.android.aktionTrocken", MODE_PRIVATE);

        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = telephonyManager.getDeviceId();

        userData = new User("0","0");

        initSpinner();
        Button btnContinue = (Button) findViewById(R.id.continueButton);
        btnContinue.setOnClickListener(this);

        cbMale = (CheckBox) findViewById(R.id.cbMale);

        cbFemale = (CheckBox) findViewById(R.id.cbFemale);

        cbMale.setOnClickListener(this);
        cbFemale.setOnClickListener(this);

        //spYear = (Spinner) findViewById(R.id.spYear);
        spCountry = (Spinner) findViewById(R.id.spCountry);
        spState = (Spinner) findViewById(R.id.spState);

        etYear = (EditText) findViewById(R.id.etYear);

       /* spYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                savePreferencesYear("Year_Value", position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                savePreferencesYear("Country_Value", position);
                if(position==1){
                    spState.setVisibility(View.VISIBLE);
                } else {
                    spState.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                savePreferencesYear("State_Value", position);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void loadPreferences() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        url = sharedPreferences.getString("url", "");
        port = sharedPreferences.getString("port", "");
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        String selectedCountry = spCountry.getSelectedItem().toString();
        if (selectedCountry.equals("Österreich")) {
            spState.setVisibility(v.VISIBLE);
        } else {
            spState.setVisibility(v.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        int position;
        String date;
        switch (v.getId()) {
            case R.id.continueButton:

                //Toast.makeText(getApplication(), "Click", Toast.LENGTH_SHORT).show();
                if(checkData() == true) {
                    new HttpAsyncTaskPost(this).execute("http://" + url + port + "/users");

                }
                break;
            case R.id.cbMale:
                if(cbFemale.isChecked()) {
                    cbFemale.toggle();
                }
                break;
            case R.id.cbFemale:
                if(cbMale.isChecked()) {
                    cbMale.toggle();
                }
        }
    }

    private class HttpAsyncTaskPost extends AsyncTask<String, Void, String> {
        private ProfileData pd;
        public HttpAsyncTaskPost(ProfileData pd) {

            this.pd = pd;
        }

        @Override
        protected String doInBackground(String... urls) {

            return POST(urls[0], userData);

        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            //Toast.makeText(getBaseContext(), "Data Sent!", Toast.LENGTH_LONG).show();
            Log.d("data send", "");
            for (int i = 0; i < demoFeature.size(); i++) {
                new HttpAsyncTaskGet(pd).execute("http://" + url + port + "/groups/" + demoGroups.get(i) + "/" + demoFeature.get(i));

            }

            new HttpAsyncTaskGetUser().execute("http://" + url + port + "/users/" + deviceId);
        }

    }

   private class HttpAsyncTaskPostMem extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return POSTMem(urls[0], countForPost);

        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            //Toast.makeText(getBaseContext(), "Data Sent!", Toast.LENGTH_LONG).show();
            Log.d("data send", "");

        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public String POST(String url, User user){
        InputStream inputStream = null;
        String result = "";
        try {

            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

            String json = "";
            String deviceId = this.deviceId;
            //String sex = userOld.getSex();
            //String year = userOld.getYear().replaceAll("\\s","");
            //String year = userOld.getYear();
            //String country = userOld.getCountry();
            //String state = userOld.getState();
            String joinDate = user.getJoinDate();

            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();

            /*jsonObject.accumulate("deviceId", this.deviceId);
            jsonObject.accumulate("sex", sex);
            jsonObject.accumulate("year", year);
            jsonObject.accumulate("country", country);
            jsonObject.accumulate("state", state);*/
            jsonObject.accumulate("deviceId", deviceId);
            jsonObject.accumulate("joinDate", joinDate);

            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();

            // ** Alternative way to convert Person object to JSON string usin Jackson Lib
            // ObjectMapper mapper = new ObjectMapper();
            // json = mapper.writeValueAsString(person);

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

            } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        // 11. return result
        return result;
    }

    public String POSTMem(String url, int i){
        InputStream inputStream = null;
        String result = "";
        try {

            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

            String json = "";
            String deviceId = this.deviceId;
            String userId = this.userId;
            String joinDate = this.joinDate;
            String groupName = demoFeature.get(countForPost);
            String groupId = this.groupId.get(countForPost);
            //String year = userOld.getYear().replaceAll("\\s","");
            //String year = userOld.getYear();
            //String country = userOld.getCountry();
            //String state = userOld.getState();
            //String joinDate = user.getJoinDate();

            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();

            //*jsonObject.accumulate("deviceId", this.deviceId);
            jsonObject.accumulate("userId", userId);
            jsonObject.accumulate("joinDate", joinDate);
            jsonObject.accumulate("deviceId", deviceId);
            jsonObject.accumulate("groupName", groupName);
            jsonObject.accumulate("groupId", groupId);

            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();

            // ** Alternative way to convert Person object to JSON string usin Jackson Lib
            // ObjectMapper mapper = new ObjectMapper();
            // json = mapper.writeValueAsString(person);

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        countForPost++;
        // 11. return result
        return result;
    }

    private Boolean checkData() {
        String selectedCountry = spCountry.getSelectedItem().toString();
        //String selectedYear = spYear.getSelectedItem().toString();
        String selectedYear = "" + etYear.getText();
        String selectedState = spState.getSelectedItem().toString();


        if (cbFemale.isChecked() == false  && cbMale.isChecked() == false){

                Toast.makeText(getApplication(), "Bitte Geschlecht eintragen", Toast.LENGTH_SHORT).show();
                return false;
        } else if(selectedYear.equals("Jahrgang wählen")) {

            Toast.makeText(getApplication(), "Bitte Jahrgang eintragen", Toast.LENGTH_SHORT).show();
            return false;
        } else if (selectedCountry.equals("Land wählen")) {

            Toast.makeText(getApplication(), "Bitte Land eintragen", Toast.LENGTH_SHORT).show();
            return false;
        }

        String sex;
        if (cbFemale.isChecked() == true) {
            sex = "weiblich";
            savePreferencesMale("Male_Value", false);
        } else {
            sex = "Männlich";
            savePreferencesMale("Male_Value", true);

        }

        String year = selectedYear;
        String country = selectedCountry;
        String state;

        if (!selectedCountry.equals("Österreich")) {
            state = "nostate";
        } else {
            country = "Oesterreich";
            state = selectedState;
            if(selectedState.equals("Oberösterreich")){state = "Oberoesterreich";}
            if(selectedState.equals("Niederösterreich")){state = "Niederoesterreich";}
            if(selectedState.equals("Kärnten")){state = "Kaernten";}
        }

        demoFeature.add(selectedYear);
        demoFeature.add(selectedCountry);
        demoFeature.add(selectedState);
        demoFeature.add(sex);

        demoGroups.add("yearOfBirth");
        demoGroups.add("country");
        demoGroups.add("federalState");
        demoGroups.add("gender");

        getDate();
        userData = new User(deviceId, joinDate);

        return true;
    }

    private void initSpinner() {
        //spYear = (Spinner) findViewById(R.id.spYear);
        spCountry = (Spinner) findViewById(R.id.spCountry);
        spState = (Spinner) findViewById(R.id.spState);
        // Create an ArrayAdapter using the string array and a default spinner layout
        /*ArrayAdapter<CharSequence> aYear = ArrayAdapter.createFromResource(this,
                R.array.year, android.R.layout.simple_spinner_item);*/
        aCountry = ArrayAdapter.createFromResource(this,
                R.array.country, android.R.layout.simple_spinner_item);
        aState = ArrayAdapter.createFromResource(this,
                R.array.stateAustria, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        //aYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        //spYear.setAdapter(aYear);
        spCountry.setAdapter(aCountry);
        spState.setAdapter(aState);

        spState.setVisibility(View.GONE);
    }
    private void savePreferencesMale(String key, boolean value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }

    private void savePreferencesYear(String key, int value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }
    private void savePreferencesCountry(String key, int value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    private void savePreferencesState(String key, int value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    private String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy", Locale.GERMAN);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        joinDate = dateFormat.format(cal.getTime());

        return joinDate;
    }

    public class HttpAsyncTaskGet extends AsyncTask<String, Void, String> {
        private ProfileData pd;
        public HttpAsyncTaskGet(ProfileData pd) {
            this.pd = pd;
        }

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result)  {
            Integer cnt = 0;
            try {
                JSONArray json = new JSONArray(result);
                JSONObject jsonobject = json.getJSONObject(0);
                groupId.add(jsonobject.getString("_id"));

            } catch (JSONException e) {
                // TODO CATCH ERROR WHEN wrong data
                this.pd.catchRestError();
            }
        }
    }

    public void catchRestError(){
        Toast.makeText(this,"REST ERROR",Toast.LENGTH_SHORT).show();
    }

    public class HttpAsyncTaskGetUser extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONArray json = new JSONArray(result);

                JSONObject jsonobject = json.getJSONObject(0);

                userId = jsonobject.getString("_id");

                for (int i = 0; i < demoFeature.size(); i++) {
                    new HttpAsyncTaskPostMem().execute("http://" + url + port + "/memberships");

                }

                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivityForResult(myIntent, 0);

                prefs.edit().putBoolean("firstrun", false).commit();
                finish();

            } catch (JSONException e) {

                throw new RuntimeException(e);
            }
        }
    }

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }
}
