package com.support.android.aktionTrocken.i;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.widget.TextView;

import com.support.android.aktionTrocken.R;

/**
 * Created by simon on 28.12.2015.
 */
public class News_activity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        ab.setDisplayHomeAsUpEnabled(true);

        String newsTitle = getIntent().getStringExtra("newsTitle");
        String newsContent = getIntent().getStringExtra("newsContent");

        TextView tvTitle = (TextView) findViewById(R.id.newsTitle);
        TextView tvContent = (TextView) findViewById(R.id.newsContent);

        tvTitle.setText(newsTitle);
        tvContent.setText(newsContent);

        tvContent.setMovementMethod(new ScrollingMovementMethod());




    }
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;

    }
}
