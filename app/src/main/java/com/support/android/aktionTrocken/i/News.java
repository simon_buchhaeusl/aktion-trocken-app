package com.support.android.aktionTrocken.i;

/**
 * Created by simon on 23.07.2015.
 */
public class News {
    private String newsTitle;
    private String newsDesc;
    private int newsIcon;

    public News(){

    }
    public News(String newsTitle, String newsDesc, int newsIcon) {
        this.newsTitle = newsTitle;
        this.newsDesc = newsDesc;
        this.newsIcon = newsIcon;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsDesc() {
        return newsDesc;
    }

    public void setNewsDesc(String newsDesc) {
        this.newsDesc = newsDesc;
    }

    public int getNewsIcon() {
        return newsIcon;
    }

    public void setNewsIcon(int newsIcon) {
        this.newsIcon = newsIcon;
    }
}
