package com.support.android.aktionTrocken.i;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.support.android.aktionTrocken.MainActivity;
import com.support.android.aktionTrocken.R;
import com.support.android.aktionTrocken.helper.Helper;
import com.support.android.aktionTrocken.menus.Settings_activity;
import com.support.android.aktionTrocken.stats.Stats;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class IListFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private NewsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";
    private ProgressBar countdown;
    private int easterDay, daysUntilFast;
    private LinearLayout countDownLayout;
    private String url;
    private String port;
    ArrayList results = new ArrayList<News>();
    public String headline, content;
    public ArrayList<String> headlineArray;
    public ArrayList<String> contentArray;
    private ProgressDialog pd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        loadPreferences();
        final ArrayList<News> newsArrayList = new ArrayList<News>();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        countDownLayout = (LinearLayout) getView().findViewById(R.id.row1include);
        countDownLayout.setVisibility(View.GONE);

        mAdapter = new NewsAdapter(results);
        mRecyclerView.setAdapter(mAdapter);

        if (Helper.isNetworkAvailable(getActivity())) {
            new HttpAsyncTask().execute("http://" + url + port + "/news");
        }
    }

    private void loadPreferences() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        url = sharedPreferences.getString("url", "");
        port = sharedPreferences.getString("port", "");
    }

    private void setData() {
        this.mAdapter.clear();

        for (int i = 0; i < headlineArray.size(); i++) {
            News n = new News(headlineArray.get(i), contentArray.get(i), R.drawable.ic_action_news);

            this.mAdapter.addItem(n, i);
        }

        this.mAdapter.notifyDataSetChanged();
        //
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.i_list_fragment, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_i, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_test:
                //Toast.makeText(getActivity(),"profil",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getActivity(), Stats.class);
                startActivity(i);

                return true;
            case R.id.menu_settings:
                Intent i2 = new Intent(getActivity(), Settings_activity.class);
                startActivity(i2);
                Toast.makeText(getActivity(), "einstellungen", Toast.LENGTH_SHORT).show();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((NewsAdapter) mAdapter).setOnItemClickListener(new NewsAdapter
                .MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Log.i(LOG_TAG, " Clicked on Item " + position);
                //Toast.makeText(getActivity(), "Clicked on Item " + position, Toast.LENGTH_SHORT).show();
                String newsTitle = headlineArray.get(position);
                String newsContent = contentArray.get(position);

                Intent i = new Intent(getActivity(), News_activity.class);
                i.putExtra("newsTitle", "" + newsTitle);
                i.putExtra("newsContent", "" + newsContent);
                startActivity(i);
            }
        });
        //Toast.makeText(getActivity(), "onresume " , Toast.LENGTH_SHORT).show();

        //Freitagsrückmeldung
       /* Calendar rightNow = Calendar.getInstance();
        int day = rightNow.get(Calendar.DAY_OF_WEEK);
        if(day==Calendar.FRIDAY){
            //Toast.makeText(getActivity(), "today is today", Toast.LENGTH_SHORT).show();
            int lastWeekStateDays = 1;
            String state = "Vorarlberg";
            int allDays = 5;

            News n = new News("Feedback","Letzte Woche hat " + state + lastWeekStateDays +
                    " Verzichtstage gesammelt. Seit dem Start von AKTION.TROCKEN sind es in " + state +
                    " insgesamt " + allDays + " Verzichtstage.", R.drawable.ic_done);

            this.mAdapter.addItem(n,mAdapter.getItemCount()+1);

        this.mAdapter.notifyDataSetChanged();

        }*/

        countdown = (ProgressBar) getView().findViewById(R.id.progressBarElement);
        final TextView leftC = (TextView) getView().findViewById(R.id.tvNotDrunk);
        TextView rightC = (TextView) getView().findViewById(R.id.tvDrunk);
        rightC.setText("Fastenzeit 2016");

        DatePicker datePicker = new DatePicker(getActivity());
        Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
        localCalendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
        int currentDayOfYear = localCalendar.get(Calendar.DAY_OF_YEAR);


        String date = getEasterSundayDate(2016);
        try {
            easterDay = getDayOfYear(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        final int fastDay = easterDay - 45;

        int restDaysOfYear = 0;
        try {
            restDaysOfYear = getDayOfYear(date) - currentDayOfYear;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (currentDayOfYear > fastDay) {
            daysUntilFast = 365 - currentDayOfYear + fastDay;

        } else {
            daysUntilFast = fastDay - currentDayOfYear;
        }
        long daysUntilFastMilli = (long) daysUntilFast * 86400000;

        //Toast.makeText(getActivity(),"Daysuntilfast" + daysUntilFast,Toast.LENGTH_SHORT).show();


        //Toast.makeText(getActivity(),"Days until easter " + restDaysOfYear,Toast.LENGTH_SHORT).show();


        //Toast.makeText(getActivity(),"milli: " + daysUntilFastMilli,Toast.LENGTH_SHORT).show();

        final CountDownTimer fastTime = new CountDownTimer(3888000000L, 86400000L) {
            @Override
            public void onTick(long millisUntilFinished) {
                //60Toast.makeText(getActivity(),"seconds remaining: " + millisUntilFinished / 1000,Toast.LENGTH_SHORT).show();
                //Toast.makeText(getActivity(),"i remaining: " + (millisUntilFinished / (1000*60*60*24)),Toast.LENGTH_SHORT).show();
                //countdown.setProgress(time);
                long day = (millisUntilFinished / (1000 * 60 * 60 * 24)) + 1;
                leftC.setText("Noch " + day + " Tage");
                long dayProgress = 45 - day;
                countdown.setProgress(Integer.parseInt("" + dayProgress));


            }

            @Override
            public void onFinish() {
                Toast.makeText(getActivity(), "finished", Toast.LENGTH_SHORT).show();
                leftC.setText("Beginn");
                countdown.setProgress(30);


            }
        };

        final CountDownTimer countDownTimer = new CountDownTimer(daysUntilFastMilli, 86400000L) {
            @Override
            public void onTick(long millisUntilFinished) {
                //60Toast.makeText(getActivity(),"seconds remaining: " + millisUntilFinished / 1000,Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity(), "i remaining: " + (millisUntilFinished / (1000 * 60 * 60 * 24)), Toast.LENGTH_SHORT).show();
                //countdown.setProgress(time);
                long day = (millisUntilFinished / (1000 * 60 * 60 * 24)) + 1;
                leftC.setText("In " + day + " Tagen");
                long dayProgress = 30 - day;
                countdown.setProgress(Integer.parseInt("" + dayProgress));


            }

            @Override
            public void onFinish() {
                Toast.makeText(getActivity(), "finished", Toast.LENGTH_SHORT).show();
                leftC.setText("Beginn");
                daysUntilFast = 0;
                countdown.setProgress(30);
                fastTime.start();
            }

        };
        countdown.setMax(30);

        countdown.setMax(30);

        if (daysUntilFast <= 30) {
            countDownLayout.setVisibility(View.VISIBLE);
            countDownTimer.start();
            Toast.makeText(getActivity(), "Timer Start", Toast.LENGTH_SHORT).show();
        }
        if (daysUntilFast == 7) {
            Toast.makeText(getActivity(), "Notif", Toast.LENGTH_SHORT).show();

            /*Notification n  = new Notification.Builder(getActivity())
                    .setContentTitle("Aktion Trocken")
                    .setContentText("In 7 Tagen startet die Fastenzeit!")
                    .setSmallIcon(R.drawable.ic_dashboard)
                    .build();

            NotificationManager notificationManager =
                    (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, n);*/


        }
        if (daysUntilFast == 0) {
            Toast.makeText(getActivity(), "Notif", Toast.LENGTH_SHORT).show();

            /*Notification n  = new Notification.Builder(getActivity())
                    .setContentTitle("Aktion Trocken")
                    .setContentText("Heute startet die Fastenzeit!")
                    .setSmallIcon(R.drawable.ic_dashboard)
                    .build();

            NotificationManager notificationManager =
                    (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, n);*/


            Intent dismissIntent = new Intent(getActivity(), MainActivity.class);
            PendingIntent piDismiss = PendingIntent.getService(getActivity(), 0, dismissIntent, 0);

            Intent snoozeIntent = new Intent(getActivity(), MainActivity.class);
            PendingIntent piSnooze = PendingIntent.getService(getActivity(), 0, snoozeIntent, 0);

            Notification.Builder n = new Notification.Builder(getActivity())
                    .setContentTitle("Aktion Trocken")
                    .setContentText("Heute TEST startet die Fastenzeit!")
                    .setSmallIcon(R.drawable.ic_dashboard)
                    .addAction(R.drawable.ic_drunk,
                            getString(R.string.consum), piDismiss)
                    .addAction(R.drawable.ic_done,
                            getString(R.string.done), piSnooze);
            ;

            //NotificationManager notificationManager =
            //(NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);
            //notificationManager.notify(0, n);
            Intent resultIntent = new Intent(getActivity(), MainActivity.class);

            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            getActivity(),
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            n.setContentIntent(resultPendingIntent);


        }


    }

    private String getDaysLeft(int i) {
        String date;
        String daysLeft;
        DateFormat dateFormat = new SimpleDateFormat("dd", Locale.GERMAN);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, i);
        date = dateFormat.format(cal.getTime());
        Toast.makeText(getActivity(), "date" + date, Toast.LENGTH_LONG).show();

        return date;
    }

    private int getDayOfYear(String dateString) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd");
        Date date = dateFormat.parse(dateString);
        GregorianCalendar greg = new GregorianCalendar();
        greg.setTime(date);

        return greg.get(greg.DAY_OF_YEAR);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Toast.makeText(getActivity(), "hidden", Toast.LENGTH_SHORT).show();

    }


    public static String getEasterSundayDate(int year) {
        int a = year % 19,
                b = year / 100,
                c = year % 100,
                d = b / 4,
                e = b % 4,
                g = (8 * b + 13) / 25,
                h = (19 * a + b - d - g + 15) % 30,
                j = c / 4,
                k = c % 4,
                m = (a + 11 * h) / 319,
                r = (2 * e + 2 * j - k - h + m + 32) % 7,
                n = (h - m + r + 90) / 25,
                p = (h - m + r + n + 19) % 32;


        String result;
        switch (n) {
            case 1:
                result = "January ";
                break;
            case 2:
                result = "February ";
                break;
            case 3:
                result = "March ";
                break;
            case 4:
                result = "April ";
                break;
            case 5:
                result = "May ";
                break;
            case 6:
                result = "June ";
                break;
            case 7:
                result = "July ";
                break;
            case 8:
                result = "August ";
                break;
            case 9:
                result = "September ";
                break;
            case 10:
                result = "October ";
                break;
            case 11:
                result = "November ";
                break;
            case 12:
                result = "December ";
                break;
            default:
                result = "error";
        }

        return result + p;
    }


    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            headlineArray = new ArrayList<String>();
            contentArray = new ArrayList<String>();
            try {
                JSONArray json = new JSONArray(result);

                for (int i = 0; i < json.length(); i++) {

                    JSONObject jsonobject = json.getJSONObject(i);

                    headline = jsonobject.getString("headline");
                    content = jsonobject.getString("content");

                    headlineArray.add(headline);
                    contentArray.add(content);
                    /*News news = new News(headline, content, R.drawable.ic_dashboard);
                    results.add(i, news);*/
                }

                Collections.reverse(headlineArray);
                Collections.reverse(contentArray);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            setData();
        }
    }
}
