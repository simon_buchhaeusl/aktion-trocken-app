package com.support.android.aktionTrocken.entry;

public class Entry {
    private int id;
    private String day;
    private String date;
    private String detailDate;
    private int consumption;
    private int situation;
    private int icon;
    private int icon2;
    private String deviceId;

    public Entry(){

    }

    public Entry(String day, String date, int consumption, int situation, int icon, int icon2, int id, String detailDate, String deviceId) {
        this.day = day;
        this.date = date;
        this.consumption = consumption;
        this.situation = situation;
        this.icon = icon;
        this.icon2 = icon2;
        this.id = id;
        this.detailDate = detailDate;
        this.deviceId = deviceId;
    }
    public Entry(String day, String date, int consumption, int situation, int icon, int icon2, String detailDate, String deviceId) {
        this.day = day;
        this.date = date;
        this.consumption = consumption;
        this.situation = situation;
        this.icon = icon;
        this.icon2 = icon2;
        this.detailDate = detailDate;
        this.deviceId = deviceId;
    }

    public String getDeviceId() { return deviceId; }

    public void setDeviceId(String deviceId) { this.deviceId = deviceId; }

    public String getDetailDate() { return detailDate; }

    public void setDetailDate(String detailDate) { this.detailDate = detailDate; }

    public void setDay(String day) {
        this.day = day;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setConsumption(int consumption) {
        this.consumption = consumption;
    }

    public void setSituation(int situation) {
        this.situation = situation;
    }

    public String getDay() {
        return day;
    }

    public String getDate() {
        return date;
    }

    public int getConsumption() {
        return consumption;
    }

    public int getSituation() {
        return situation;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getIcon2() {
        return icon2;
    }

    public void setIcon2(int icon2) {
        this.icon2 = icon2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
