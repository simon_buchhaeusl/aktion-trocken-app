package com.support.android.aktionTrocken.menus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.support.android.aktionTrocken.MainActivity;
import com.support.android.aktionTrocken.R;

/**
 * Created by simon on 05.08.2015.
 */
public class Profile_activity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    CheckBox cbMale, cbFemale;
    Spinner spYear, spCountry, spState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        ab.setDisplayHomeAsUpEnabled(true);

        initSpinner();

        cbMale = (CheckBox) findViewById(R.id.cbMale);
        cbFemale = (CheckBox) findViewById(R.id.cbFemale);

        cbMale.setOnClickListener(this);
        cbFemale.setOnClickListener(this);

        spCountry.setOnItemSelectedListener(this);

        spCountry.setOnItemSelectedListener(this);
        cbMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                savePreferencesMale("Male_Value", cbMale.isChecked());
            }
        });
        spYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                savePreferencesYear("Year_Value", position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                savePreferencesCountry("Country_Value", position);
                if(position == 1){
                    spState.setVisibility(View.VISIBLE);
                }else {
                    spState.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                savePreferencesState("State_Value", position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        loadSavedPreferences();

    }

    private void loadSavedPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean checkBoxValue = sharedPreferences.getBoolean("Male_Value", true);
        int yearPos = sharedPreferences.getInt("Year_Value", 0);
        int countryPos = sharedPreferences.getInt("Country_Value", 0);
        int statePos = sharedPreferences.getInt("State_Value", 0);


        //String name = sharedPreferences.getString("storedName", "YourName");

        if (checkBoxValue) {
            cbMale.setChecked(true);
            cbFemale.setChecked(false);
        } else {
            cbMale.setChecked(false);
            cbFemale.setChecked(true);
        }
        spYear.setSelection(yearPos);
        spCountry.setSelection(countryPos);
        if(countryPos == 1) {
            spState.setVisibility(View.VISIBLE);
            spState.setSelection(statePos);
        } else {
            spState.setVisibility(View.GONE);
        }


    }

    private void savePreferencesMale(String key, boolean value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }

    private void savePreferencesYear(String key, int value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }
    private void savePreferencesCountry(String key, int value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    private void savePreferencesState(String key, int value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        String selectedCountry = spCountry.getSelectedItem().toString();

        if (selectedCountry.equals("Österreich")) {
            spState.setVisibility(v.VISIBLE);
        } else {
            spState.setVisibility(v.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        int position;
        String date;
        switch (v.getId()) {
            case R.id.continueButton:

                //Toast.makeText(getApplication(), "Click", Toast.LENGTH_SHORT).show();
                if(checkData() == true) {
                    //new HttpAsyncTaskPost().execute("http://" + url + port + "/users");

                    Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivityForResult(myIntent, 0);

                    //prefs.edit().putBoolean("firstrun", false).commit();
                    finish();
                }
                break;
            case R.id.cbMale:
                if(cbFemale.isChecked()) {
                    cbFemale.toggle();
                }
                break;
            case R.id.cbFemale:
                if(cbMale.isChecked()) {
                    cbMale.toggle();
                }
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        finish();
        return true;

    }

    private Boolean checkData() {
        String selectedCountry = spCountry.getSelectedItem().toString();
        String selectedYear = spYear.getSelectedItem().toString();
        String selectedState = spState.getSelectedItem().toString();

        if (cbFemale.isChecked() == false  && cbMale.isChecked() == false){

            Toast.makeText(getApplication(), "Bitte Geschlecht eintragen", Toast.LENGTH_SHORT).show();
            return false;
        } else if(selectedYear.equals("Jahrgang wählen")) {

            Toast.makeText(getApplication(), "Bitte Jahrgang eintragen", Toast.LENGTH_SHORT).show();
            return false;
        } else if (selectedCountry.equals("Land wählen")) {

            Toast.makeText(getApplication(), "Bitte Land eintragen", Toast.LENGTH_SHORT).show();
            return false;
        }

        String sex;
        if (cbFemale.isChecked() == true) {
            sex = "weiblich";
        } else {
            sex = "maennlich";
        }

        String year = selectedYear;
        String country = selectedCountry;
        String state;

        if (!selectedCountry.equals("Österreich")) {
            state = "nostate";
        } else {
            country = "Oesterreich";
            state = selectedState;
            if(selectedState.equals("Oberösterreich")){state = "Oberoesterreich";}
            if(selectedState.equals("Niederösterreich")){state = "Niederoesterreich";}
            if(selectedState.equals("Kärnten")){state = "Kaernten";}
        }

        //userData = new UserOld(0, sex, country, state, "", year);

        return true;
    }

    private void initSpinner() {
        spYear = (Spinner) findViewById(R.id.spYear);
        spCountry = (Spinner) findViewById(R.id.spCountry);
        spState = (Spinner) findViewById(R.id.spState);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> aYear = ArrayAdapter.createFromResource(this,
                R.array.year, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> aCountry = ArrayAdapter.createFromResource(this,
                R.array.country, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> aState = ArrayAdapter.createFromResource(this,
                R.array.stateAustria, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        aYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spYear.setAdapter(aYear);
        spCountry.setAdapter(aCountry);
        spState.setAdapter(aState);

        spState.setVisibility(View.GONE);
    }


}
